#!"c:\users\tcjk47\git\leetcode\venv\scripts\python.exe"

import sys
import argparse
from pprint import pformat
import boto3


def parse_args(args):
    parser = argparse.ArgumentParser()
    group_id = parser.add_mutually_exclusive_group(required=True)
    group_option = parser.add_mutually_exclusive_group(required=False)
    group_id.add_argument('-i', '--instance_id',
                          help=('Pass the EC2 instance id'))
    group_id.add_argument('-d', '--default_id', action='store_const', const='i-0d60131e5375ef7e4',
                          help=('The default instance id will be used'))
    group_option.add_argument('-s', '--start', action='store_true',
                              help=('start the instance'))
    group_option.add_argument('-p', '--stop', action='store_true',
                              help=('stop the instance'))
    return parser.parse_args()


def main():
    args = parse_args(sys.argv)
    ec2 = boto3.client('ec2')
    instance_id = args.default_id or args.instance_id

    manage_ec2(ec2, instance_id, args)


def manage_ec2(ec2, instance_id, args):
    if args.start:
        start_ec2(ec2,instance_id)
    elif args.stop:
        stop_ec2(ec2,instance_id)
    else:
        print_info(instance_id)

def start_ec2(ec2, instance_id):
    print("Starting instance {}".format(instance_id))
    print(ec2.start_instances(InstanceIds=[instance_id]))


def stop_ec2(ec2, instance_id):
    print("Stoping instance {}".format(instance_id))
    print(ec2.stop_instances(InstanceIds=[instance_id]))


def print_info(instance_id):
    print("I did nothing...")
    print("The id is {}".format(instance_id))


if __name__ == '__main__':
    sys.exit(main())
